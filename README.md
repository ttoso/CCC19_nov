# Catalysts Coding Contest 2019

This is a repository for the CCC 2019 (November) solutions that my group made.

## Who we are

* **Group name:** Process of Rage
* **Member #1:** Carlos Gomez Hernandez [(Github)](https://github.com/kurolox)
* **Member #2:** Carlos Lobo Mata [(Github)](https://github.com/clm2609)
* **Member #3:** Antonio Roman Lopez [(Github)](https://github.com/ttoso)

## Folder structure

* `src`: All source code files (scripts, classes...)
* `docs`: All of the PDFs explaining each level problem.
* `inputs`: Directory where all the provided inputs are stored. All the files inside are assumed to be named `levelX_Y.in`.
* `outputs`: Directory where all the generated outputs are stored. All the files generated inside will be named `levelX_Y.out`.


